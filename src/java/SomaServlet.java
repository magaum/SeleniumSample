/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * Esta rota (/somar) é onde o formulário criado no arquivo index.html envia os dados após a submissão
 */

@WebServlet(urlPatterns = {"/somar"})
public class SomaServlet extends HttpServlet {

    
protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            response.setContentType("text/html;charset=UTF-8");
            
            int p1 = Integer.parseInt(request.getParameter("p1"));
            int p2 = Integer.parseInt(request.getParameter("p2"));
            int total = p1+p2;
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Resultado da soma</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>O resultado é: " + total + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

}
