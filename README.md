# How to use this project

Basic instructions for beginners

## Execution

1. Create an web project (this project was created in Netbeans 8.2);
2. Add the files on the same directories src and web;
3. Run
4. The parameters **p1** and **p2** are past by an query string on format below:

HTTP Pattern

protocol://server:port/projectName/resource?parameter=value&parameter=value

The parameters are split by **&**

e.g.: http://localhost:8080/SeleniumSample/somar?p1=15&p2=5&Calcular=Submit

|HTTP Pattern|Example|
|---|---|
|protocol|http|
|server|localhost|
|port|8080|
|projectName|SeleniumSample|
|resource|somar|
|parameter|p1|
|value|15|
|parameter|p2|
|value|5|
|parameter|Calcular|
|value|Submit|

## Selenium

How to use Selenium IDE

Chrome example:

1 Add the selenium IDE on [Chrome or chromium](https://chrome.google.com/webstore/detail/katalon-recorder-selenium/ljdobmomdgdljniojadhoplhkpialdid)

![Selenium](files/imgsReadMe/1.png)

2 Run the application:

![Application running](files/imgsReadMe/2.png)

3 Click on selenium IDE extension:

![Selenium IDE](files/imgsReadMe/3.png)

4 Start selenium and click on the red button or press Ctrl+U:

![Selenium running](files/imgsReadMe/4.png)

5 Type the values of form and continue:

![Form](files/imgsReadMe/5.png)

6 Select the final result and click with the right button as showed below:

![Operation Result](files/imgsReadMe/6.png)

7 See selenium test:

![Selenium test](files/imgsReadMe/7.png)

8 Click on run button or type Ctrl+R:

![Selenium test](files/imgsReadMe/8.png)

9 It's possible export the selenium test to other languages:

![Selenium export](files/imgsReadMe/9.png)

---

**Troubleshooting**

Exported tests does not work correctly:

1. Verify your [geckodriver](https://firefox-source-docs.mozilla.org/testing/geckodriver/geckodriver/index.html#)
2. If he do not exist in your computer it's possible to download [here](https://github.com/mozilla/geckodriver/releases)
3. Error **java.lang.IllegalStateException: The path to the driver executable must be set by the webdriver.gecko.driver system property;**
    * The line below can solve the problem, if the steps one and two are ok.

    ~~~Java
    System.setProperty("webdriver.gecko.driver","path of geckodriver");
    ~~~

    e.g Linux:

    ~~~Java
    System.setProperty("webdriver.gecko.driver","/home/user/geckodriver");
    ~~~

    e.g Windows:

    ~~~Java
    System.setProperty("webdriver.gecko.driver","c:\geckodriver.exe");
    ~~~